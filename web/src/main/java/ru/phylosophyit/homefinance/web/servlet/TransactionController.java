package ru.phylosophyit.homefinance.web.servlet;

import ru.phylosophyit.homefinance.dao.model.AccountUsersModel;
import ru.phylosophyit.homefinance.dao.model.CategoryModel;
import ru.phylosophyit.homefinance.dao.model.TransactionModel;
import ru.phylosophyit.homefinance.dao.repository.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/UserController")
public class TransactionController extends HttpServlet {
    private final static String INSERT_OR_EDIT = "/jsp/transaction_.jsp";
    private final static String LIST_USER = "/jsp/listTransaction.jsp";
    private TransactionRepository transactionRepository;
    private ConnectionSupplier connectionSupplier;

    @Override
    public void init() {
        connectionSupplier = new ConnectionSupplier();
        transactionRepository = new TransactionRepository(connectionSupplier);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward;
        String action = request.getParameter("action");
        System.out.println(action);
        if (action.equalsIgnoreCase("delete")) {
            int transactionId = Integer.parseInt(request.getParameter("transactionId"));
            transactionRepository.remove(transactionId);
            forward = LIST_USER;
            request.setAttribute("transactions", transactionRepository.findAll());
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            long transactionId = Integer.parseInt(request.getParameter("transactionId"));
            TransactionModel transaction = transactionRepository.findByID(transactionId);
            request.setAttribute("transaction", transaction);
        } else if (action.equalsIgnoreCase("listTransactions")) {
//            System.out.println(transactionRepository.findAll());
            forward = LIST_USER;
            request.setAttribute("transactions", transactionRepository.findAll());
        } else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TransactionModel transactionModel = new TransactionModel();
        request.setCharacterEncoding("UTF-8");
        List<Long> categoryModelsId;
        String transactionId = request.getParameter("id");
        String categoryModelsIdStr = request.getParameter("categoryModelsId");

        transactionModel.setName(request.getParameter("name"));
        transactionModel.setAmount(new BigDecimal(request.getParameter("amount")));
        transactionModel.setProfitOrLoss(Boolean.parseBoolean(request.getParameter("profitOrLoss")));
        transactionModel.setTransactionType(
                new TransactionTypeRepository(connectionSupplier)
                        .findByID(Integer.parseInt(request.getParameter("transactionTypeId"))));
        transactionModel.setAccountUsersModel(
                new AccountUsersRepository(connectionSupplier)
                        .findByID(Integer.parseInt(request.getParameter("accountUsersId"))));
        transactionModel.setId(Long.parseLong(transactionId));

        if (!categoryModelsIdStr.isEmpty()) {
            categoryModelsId = Arrays.stream(request.getParameter("categoryModelsId").split(",")).map(Long::valueOf).collect(Collectors.toList());
            Set<CategoryModel> categoryModels = new HashSet<>();
            for (long x : categoryModelsId) {
                categoryModels.add(new CategoryRepository(connectionSupplier).findByID(x));
            }
            transactionModel.setCategoryModels(categoryModels);
        } else transactionModel.setCategoryModels(null);

        if (transactionId.isEmpty()) transactionRepository.save(transactionModel);
        else transactionRepository.update(transactionModel);

        System.out.println(transactionModel);
        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
        request.setAttribute("transactions", transactionRepository.findAll());
        view.forward(request, response);
    }

    @Override
    public void destroy() {
        connectionSupplier = null;
        transactionRepository = null;
    }

}

package ru.phylosophyit.homefinance.web.servlet;

import ru.phylosophyit.homefinance.dao.repository.ConnectionSupplier;
import ru.phylosophyit.homefinance.dao.repository.TransactionRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/transaction")
public class MainServlet extends HttpServlet {
    private TransactionRepository transactionRepository;
    private ConnectionSupplier connectionSupplier;
    private static final long serialVersionUID = 1L;

    @Override
    public void init() {
        System.out.println("Use new connection");
        System.out.println("Use new transactionRepository");
        connectionSupplier = new ConnectionSupplier();
        transactionRepository = new TransactionRepository(connectionSupplier);
    }

    public MainServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        long id = Integer.parseInt(req.getParameter("id"));
        RequestDispatcher rd = req.getServletContext().getRequestDispatcher("/jsp/transaction.jsp");
        System.out.println("Start");
        try {
            req.setAttribute("transaction", transactionRepository.findByID(id));
        } catch (Exception e) {
            req.setAttribute("transaction", "No such transaction!!! " + e.getMessage());
        }
        rd.forward(req, resp);
        System.out.println("End");
    }

    @Override
    public void destroy() {
        System.out.println("Destroy connection");
        transactionRepository = null;
        connectionSupplier = null;
    }
}

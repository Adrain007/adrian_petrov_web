<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: adpetrov
  Date: 14.02.2021
  Time: 22:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add new transaction</title>
</head>
<body>
<form method="POST" action='UserController' name="frmAddUser">
    ID : <input
        type="text" name="id" readonly="readonly"
        value="<c:out value="${transaction.id}" />"/> <br/>
    Name : <input
        type="text" name="name"
        value="<c:out value="${transaction.name}" />"/> <br/>
    Amount : <input
        type="number" name="amount"
        value="<c:out value="${transaction.amount}" />"/> <br/>
    ProfitOrLoss : <input
        type="text" name="profitOrLoss"
        value="<c:out value="${transaction.profitOrLoss}" />"/> <br/>
    TransactionTypeId : <input type="text" name="transactionTypeId"
                               value="<c:out value="${transaction.transactionType.id}" />"/> <br/>
    AccountUsersId : <input type="text" name="accountUsersId"
                            value="<c:out value="${transaction.accountUsersModel.id}" />"/> <br/>
    <c:forEach items="${transaction.categoryModels}" var="row">
        <c:if test="${not empty buf}">
            <c:set var="buf" value="${buf}, ${row.id}"/>
        </c:if>
        <c:if test="${empty buf}">
            <c:set var="buf" value="${row.id}"/>
        </c:if>
    </c:forEach>
    CategoryModelsId : <input type="text" name="categoryModelsId" value="<c:out value="${buf}" />"/> <br/>
    <input type="submit" value="Submit"/>
</form>
</body>
</html>

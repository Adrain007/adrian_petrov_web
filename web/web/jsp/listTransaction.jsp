<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<%--
  Created by IntelliJ IDEA.
  User: adpetrov
  Date: 14.02.2021
  Time: 22:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Show All Transactions</title>
</head>
<body>
    <table border="1">
        <thead >
            <tr>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Profit_Or_Loss</th>
                <th>Transaction_Type_ID</th>
                <th>Account_Users_Model_ID</th>
                <th>CategoryModels</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
<%--<p>Hello</p>--%>
        <%--@elvariable id="transactions" type="java.util.List"--%>
        <c:forEach items="${transactions}" var="transaction">
<%--            ${transaction.name}<br>--%>
            <tr>
                <td><c:out value="${transaction.id}"/></td>
                <td><c:out value="${transaction.name}"/></td>
                <td><c:out value="${transaction.amount}"/></td>
                <td><c:out value="${transaction.profitOrLoss}"/></td>
                <td><c:out value="${transaction.transactionType.id}"/></td>
                <td><c:out value="${transaction.accountUsersModel.id}"/></td>
                <c:forEach items="${transaction.categoryModels}" var="row">
                    <c:if test="${not empty buf}">
                        <c:set var="buf" value="${buf}, ${row.id}"/>
                    </c:if>
                    <c:if test="${empty buf}">
                        <c:set var="buf" value="${row.id}"/>
                    </c:if>
                </c:forEach>
                <td>${buf}</td><c:set var="buf" value=""/>
<%--                <td><c:out value="${transaction.categoryModels}"/></td>--%>
                <td><a href="UserController?action=edit&transactionId=<c:out value="${transaction.id}"/>">Update</a></td>
                <td><a href="UserController?action=delete&transactionId=<c:out value="${transaction.id}"/>">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <p><a href="UserController?action=insert">Add Transaction</a></p>
</body>
</html>

package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.CurrencyModel;
import ru.phylosophyit.homefinance.dao.repository.ConnectionSupplier;
import ru.phylosophyit.homefinance.dao.repository.CurrencyRepository;

public class CurrencyRepositoryTest {
    private CurrencyRepository currencyRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        currencyRepository = new CurrencyRepository(connectionSupplier);
    }

    @Test
    public void testCategoryRepositoryFindByIdAndSave() {
        CurrencyModel expected = new CurrencyModel("Test", "6", "?");

        expected.setId(currencyRepository.save(expected).getId());
        CurrencyModel actual = currencyRepository.findByID(expected.getId());

        Assert.assertEquals(expected, actual);

        currencyRepository.remove(actual.getId());
    }

    @Test
    public void testCategoryRepositoryUpdate() {
        CurrencyModel expected = new CurrencyModel("Test", "6", "?");
        expected = currencyRepository.save(expected);
        expected.setName("LL");
        currencyRepository.update(expected);

        Assert.assertEquals("LL", currencyRepository.findByID
                (expected.getId()).getName());
        currencyRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testCategoryRepositoryRemove() {
        CurrencyModel expected = new CurrencyModel("Test", "6", "?");
        expected = currencyRepository.save(expected);

        currencyRepository.remove(expected.getId());
        currencyRepository.findByID(expected.getId());
    }
}

package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.ParentCategoryModel;
import ru.phylosophyit.homefinance.dao.repository.ConnectionSupplier;
import ru.phylosophyit.homefinance.dao.repository.ParentCategoryRepository;

public class ParentCategoryRepositoryTest {
    private ParentCategoryRepository parentCategoryRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
    }

    @Test
    public void testCategoryRepositoryFindByIdAndSave() {
        ParentCategoryModel expected = new ParentCategoryModel("Тестовые товары");

        expected.setId(parentCategoryRepository.save(expected).getId());
        ParentCategoryModel actual = parentCategoryRepository.findByID(expected.getId());

        Assert.assertEquals(expected, actual);

        parentCategoryRepository.remove(actual.getId());
    }

    @Test
    public void testCategoryRepositoryUpdate() {
        ParentCategoryModel expected = new ParentCategoryModel("Тестовые товары");
        expected = parentCategoryRepository.save(expected);
        expected.setName("LL");
        parentCategoryRepository.update(expected);

        Assert.assertEquals("LL", parentCategoryRepository.findByID
                (expected.getId()).getName());
        parentCategoryRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testCategoryRepositoryRemove() {
        ParentCategoryModel expected = new ParentCategoryModel("Тестовые товары");
        expected = parentCategoryRepository.save(expected);

        parentCategoryRepository.remove(expected.getId());
        parentCategoryRepository.findByID(expected.getId());
    }
}

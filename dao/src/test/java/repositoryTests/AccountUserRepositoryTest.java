package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.AccountUsersModel;
import ru.phylosophyit.homefinance.dao.model.CurrencyModel;
import ru.phylosophyit.homefinance.dao.repository.*;

import java.math.BigDecimal;

public class AccountUserRepositoryTest {
    private AccountUsersRepository accountUsersRepository;
    private CurrencyRepository currencyRepository;


    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        currencyRepository = new CurrencyRepository(connectionSupplier);
        accountUsersRepository = new AccountUsersRepository(connectionSupplier);
    }

    private AccountUsersModel fillingTransactionModel (){
        CurrencyModel currencyModel = currencyRepository.findByID(1);
        return new AccountUsersModel("Тест", new BigDecimal(23543), currencyModel);
    }

    @Test
    public void testCategoryRepositoryFindByIdAndSave() {
        AccountUsersModel expected = fillingTransactionModel();

        expected.setId(accountUsersRepository.save(expected).getId());
        AccountUsersModel actual = accountUsersRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        accountUsersRepository.remove(actual.getId());
    }

    @Test
    public void testCategoryRepositoryUpdate() {
        AccountUsersModel expected = fillingTransactionModel();
        expected = accountUsersRepository.save(expected);
        expected.setName("Мы поменяли на корм для животных");
        accountUsersRepository.update(expected);

        Assert.assertEquals("Мы поменяли на корм для животных", accountUsersRepository.findByID
                (expected.getId()).getName());
        accountUsersRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testCategoryRepositoryRemove() {
        AccountUsersModel expected = fillingTransactionModel();
        expected = accountUsersRepository.save(expected);

        accountUsersRepository.remove(expected.getId());
        accountUsersRepository.findByID(expected.getId());
    }
}

package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.TransactionType;
import ru.phylosophyit.homefinance.dao.repository.ConnectionSupplier;
import ru.phylosophyit.homefinance.dao.repository.TransactionTypeRepository;

public class TransactionTypeRepositoryTest {
    private TransactionTypeRepository transactionTypeRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
    }

    @Test
    public void testCategoryRepositoryFindByIdAndSave() {
        TransactionType expected = new TransactionType("Тестовый тип");
        expected.setId(transactionTypeRepository.save(expected).getId());
        TransactionType actual = transactionTypeRepository.findByID(expected.getId());
        Assert.assertEquals(expected, actual);
        transactionTypeRepository.remove(actual.getId());
    }

    @Test
    public void testCategoryRepositoryUpdate() {
        TransactionType expected = new TransactionType("Тестовый тип");
        expected = transactionTypeRepository.save(expected);
        expected.setName("LL");
        transactionTypeRepository.update(expected);

        Assert.assertEquals("LL", transactionTypeRepository.findByID
                (expected.getId()).getName());
        transactionTypeRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testCategoryRepositoryRemove() {
        TransactionType expected = new TransactionType("Тестовый тип");
        expected = transactionTypeRepository.save(expected);

        transactionTypeRepository.remove(expected.getId());
        transactionTypeRepository.findByID(expected.getId());
    }
}

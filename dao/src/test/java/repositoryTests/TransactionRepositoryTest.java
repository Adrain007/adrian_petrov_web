package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.*;
import ru.phylosophyit.homefinance.dao.repository.*;

import java.math.BigDecimal;

public class TransactionRepositoryTest {
    private TransactionTypeRepository transactionTypeRepository;
    private AccountUsersRepository accountUsersRepository;
    private TransactionRepository transactionRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
        accountUsersRepository = new AccountUsersRepository(connectionSupplier);
        transactionRepository = new TransactionRepository(connectionSupplier);
    }

    private TransactionModel fillingTransactionModel (){
        AccountUsersModel accountUsersModel = accountUsersRepository.findByID(1);
        TransactionType transactionType = transactionTypeRepository.findByID(1);

        return new TransactionModel("Долг",new BigDecimal(2934023),true,transactionType,accountUsersModel);
    }

    @Test
    public void testCategoryRepositoryFindByIdAndSave() {
        TransactionModel expected = fillingTransactionModel();

        expected.setId(transactionRepository.save(expected).getId());
        TransactionModel actual = transactionRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        transactionRepository.remove(actual.getId());
    }

    @Test
    public void testCategoryRepositoryUpdate() {
        TransactionModel expected = fillingTransactionModel();

        expected = transactionRepository.save(expected);
        expected.setName("Мы поменяли на выручку");
        transactionRepository.update(expected);

        Assert.assertEquals("Мы поменяли на выручку", transactionRepository.findByID
                (expected.getId()).getName());
        transactionRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testCategoryRepositoryRemove() {
        TransactionModel expected = fillingTransactionModel();

        expected = transactionRepository.save(expected);

        transactionRepository.remove(expected.getId());
        transactionRepository.findByID(expected.getId());
    }
}

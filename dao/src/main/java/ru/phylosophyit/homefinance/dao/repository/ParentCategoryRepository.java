package ru.phylosophyit.homefinance.dao.repository;

import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.ParentCategoryModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class ParentCategoryRepository implements Repository<ParentCategoryModel> {

    private final ConnectionSupplier connectionSupplier;

    public ParentCategoryRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public ParentCategoryModel findByID(long id) {
        Connection connection = connectionSupplier.connection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE id = ?");
            preparedStatement.setLong(1, id);
            return getParentCategoryModel(preparedStatement);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID ParentCategoryRepository", e);
        }
    }

    private ParentCategoryModel getParentCategoryModel(PreparedStatement preparedStatement) throws SQLException {
        ParentCategoryModel parentCategoryModel = new ParentCategoryModel();
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        parentCategoryModel.setId(resultSet.getLong(1));
        parentCategoryModel.setName(resultSet.getString(2));
        return parentCategoryModel;
    }

    @Override
    public ParentCategoryModel findByName(String name) {
        Connection connection = connectionSupplier.connection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE name = " + "'" + name + "'");
            return getParentCategoryModel(preparedStatement);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByName ParentCategoryRepository", e);
        }
    }

    @Override
    public Collection<ParentCategoryModel> findAll() {

        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM parent_category_tbl")) {
            ArrayList<ParentCategoryModel> parentCategoryModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ParentCategoryModel parentCategoryModel = new ParentCategoryModel();
                parentCategoryModel.setId(resultSet.getLong(1));
                parentCategoryModel.setName(resultSet.getString(2));
                parentCategoryModels.add(parentCategoryModel);
            }
            return parentCategoryModels;
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll ParentCategoryRepository", e);
        }
    }

    @Override
    public void remove(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM parent_category_tbl WHERE id = ?")) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove ParentCategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove ParentCategoryRepository", e);
        }
    }

    @Override
    public ParentCategoryModel save(ParentCategoryModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO parent_category_tbl (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save ParentCategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save ParentCategoryRepository", e);
        }
    }

    @Override
    public void update(ParentCategoryModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE parent_category_tbl SET name = ? WHERE ID = ?")) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setLong(2, model.getId());
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in update ParentCategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in update ParentCategoryRepository", e);
        }
    }
}

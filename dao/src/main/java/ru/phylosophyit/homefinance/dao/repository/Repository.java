package ru.phylosophyit.homefinance.dao.repository;

import java.util.Collection;

public interface Repository<T> {

    T findByID(long id);

    T findByName(String name);

    Collection<T> findAll();

    void remove(long id);

    T save(T model);

    void update(T model);

}

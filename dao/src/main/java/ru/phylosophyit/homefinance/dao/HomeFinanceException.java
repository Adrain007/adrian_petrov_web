package ru.phylosophyit.homefinance.dao;

public class HomeFinanceException extends RuntimeException {
    public HomeFinanceException(String message, Throwable exception){
        super(message,exception);
    }
}

package ru.phylosophyit.homefinance.dao.repository;

import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.AccountUsersModel;
import ru.phylosophyit.homefinance.dao.model.CurrencyModel;


import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class AccountUsersRepository implements Repository<AccountUsersModel> {
    private final ConnectionSupplier connectionSupplier;

    public AccountUsersRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public AccountUsersModel findByID(long id) {
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM accountmodel_tbl WHERE id = ?")) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return getAccountUsersModel(preparedStatement);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID AccountUsersRepository", e);
        }
    }

    private AccountUsersModel getAccountUsersModel(PreparedStatement preparedStatement) throws SQLException {
        AccountUsersModel accountUsersModel = new AccountUsersModel();
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        accountUsersModel.setId(resultSet.getLong(1));
        accountUsersModel.setName(resultSet.getString(2));
        accountUsersModel.setAmount(resultSet.getBigDecimal(3));
        long currencyId = resultSet.getLong(4);
        CurrencyModel currencyModel = new CurrencyRepository(connectionSupplier).findByID(currencyId);
        accountUsersModel.setCurrencyModel(currencyModel);
        return accountUsersModel;
    }

    @Override
    public AccountUsersModel findByName(String name) {
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM accountmodel_tbl WHERE name = ?")) {
            preparedStatement.setString(1, name);
            return getAccountUsersModel(preparedStatement);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByName AccountUsersRepository", e);
        }
    }

    @Override
    public Collection<AccountUsersModel> findAll() {
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM accountmodel_tbl")) {
            ArrayList<AccountUsersModel> accountUsersModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                AccountUsersModel accountUsersModel = new AccountUsersModel();
                accountUsersModel.setId(resultSet.getLong(1));
                accountUsersModel.setName(resultSet.getString(2));
                accountUsersModel.setAmount(resultSet.getBigDecimal(3));
                long currencyId = resultSet.getLong(4);
                CurrencyModel currencyModel = new CurrencyRepository(connectionSupplier).findByID(currencyId);
                accountUsersModel.setCurrencyModel(currencyModel);
                accountUsersModels.add(accountUsersModel);
            }
            return accountUsersModels;
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll AccountUsersRepository", e);
        }
    }

    @Override
    public void remove(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM accountmodel_tbl WHERE id = ?")) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove AccountUsersRepository", e);
        }
    }

    @Override
    public AccountUsersModel save(AccountUsersModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO accountmodel_tbl (name, amount, currency_id) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setBigDecimal(2, model.getAmount());
                preparedStatement.setLong(3, model.getCurrencyModel().getId());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save AccountUsersRepository", e);
        }
    }

    @Override
    public void update(AccountUsersModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE accountmodel_tbl SET name = ?, amount = ?, currency_id = ? WHERE ID = ?")) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setBigDecimal(2, model.getAmount());
                preparedStatement.setLong(3, model.getCurrencyModel().getId());
                preparedStatement.setLong(4, model.getId());
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in update AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in update AccountUsersRepository", e);
        }
    }
}

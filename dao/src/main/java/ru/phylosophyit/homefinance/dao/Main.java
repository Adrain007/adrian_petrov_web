package ru.phylosophyit.homefinance.dao;

import ru.phylosophyit.homefinance.dao.model.CategoryModel;
import ru.phylosophyit.homefinance.dao.model.TransactionModel;
import ru.phylosophyit.homefinance.dao.repository.CategoryRepository;
import ru.phylosophyit.homefinance.dao.repository.ConnectionSupplier;
import ru.phylosophyit.homefinance.dao.repository.ParentCategoryRepository;
import ru.phylosophyit.homefinance.dao.repository.TransactionRepository;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String... args) {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        CategoryRepository categoryRepository = new CategoryRepository(connectionSupplier);
        TransactionRepository transactionRepository = new TransactionRepository(connectionSupplier);
        ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
        Set<TransactionModel> transactionModels = new HashSet<>();
        transactionModels.add(transactionRepository.findByID(1));
        CategoryModel categoryModel = new CategoryModel("Тестовая категория", parentCategoryRepository.findByID(1), transactionModels);
//        System.out.println(categoryRepository.findAll());
        new Main().pattern(7);
    }

    private void pattern(int n) {
        for (int i = 0; i < n; i++) {
            int l = n - 1 - i;
            int r = n - 1 + i;
            for (int j = 0; j < 2 * n - 1; j++) {
                if (l == j || r == j) {
                    System.out.print(i + 1);
                } else if (j > l && j < r) {
                    System.out.print(0);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}

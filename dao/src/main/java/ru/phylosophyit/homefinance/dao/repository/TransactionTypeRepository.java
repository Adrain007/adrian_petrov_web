package ru.phylosophyit.homefinance.dao.repository;

import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.TransactionType;

import java.sql.*;
import java.util.ArrayList;

public class TransactionTypeRepository implements Repository<TransactionType> {
    private final ConnectionSupplier connectionSupplier;

    public TransactionTypeRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public TransactionType save(TransactionType model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_type_tbl (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save TransactionTypeRepository", e);
        }
    }

    @Override
    public void remove(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_type_tbl WHERE id = ?")) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove TransactionTypeRepository", e);
        }
    }

    @Override
    public TransactionType findByID(long id) {
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM transaction_type_tbl WHERE id = ?")) {
            preparedStatement.setLong(1, id);
            return getTransactionType(preparedStatement);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID TransactionTypeRepository", e);
        }
    }

    private TransactionType getTransactionType(PreparedStatement preparedStatement) throws SQLException {
        TransactionType transactionType = new TransactionType();
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        transactionType.setId(resultSet.getInt(1));
        transactionType.setName(resultSet.getString(2));
        return transactionType;
    }

    @Override
    public TransactionType findByName(String name) {
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM transaction_type_tbl WHERE name = ?")) {
            preparedStatement.setString(1, name);
            return getTransactionType(preparedStatement);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByName TransactionTypeRepository", e);
        }
    }

    @Override
    public void update(TransactionType model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_type_tbl SET name = ? WHERE ID = ?")) {

                preparedStatement.setString(1, model.getName());
                preparedStatement.setLong(2, model.getId());
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in update TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in update TransactionTypeRepository", e);
        }
    }

    @Override
    public ArrayList<TransactionType> findAll() {
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM transaction_type_tbl")) {
            ArrayList<TransactionType> transactionTypes = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                TransactionType transactionType = new TransactionType();
                transactionType.setId(resultSet.getInt(1));
                transactionType.setName(resultSet.getString(2));
                transactionTypes.add(transactionType);
            }
            return transactionTypes;
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll TransactionTypeRepository", e);
        }
    }
}

package ru.phylosophyit.homefinance.dao.repository;

import org.postgresql.ds.PGSimpleDataSource;
import ru.phylosophyit.homefinance.dao.HomeFinanceException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSupplier {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/etl_sys?useSSL=false&serverTimezone=UTC";
    private static final String DB_HOST = "localhost";
    private static final String DB_NAME = "etl_sys";
    private static final String DB_USER = "etl_sys";
    private static final String DB_PASSWORD = "postgres";

    public Connection connection() {

        try {
//            InitialContext initContext = new InitialContext();
            PGSimpleDataSource ds = new PGSimpleDataSource();
            ds.setServerName(DB_HOST);
            ds.setDatabaseName(DB_NAME);
            ds.setUser(DB_USER);
            ds.setPassword(DB_PASSWORD);
//            DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
            try {
                Connection conn = ds.getConnection(); //DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)
                conn.setAutoCommit(false);
                return conn;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in ConnectionSupplier", e);
            }
        } catch (Exception e) {
            throw new HomeFinanceException("Error in ConnectionSupplier", e);
        }
    }
}
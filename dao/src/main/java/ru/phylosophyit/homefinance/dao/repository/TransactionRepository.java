package ru.phylosophyit.homefinance.dao.repository;

import ru.phylosophyit.homefinance.dao.model.AccountUsersModel;
import ru.phylosophyit.homefinance.dao.model.CategoryModel;
import ru.phylosophyit.homefinance.dao.model.TransactionModel;
import ru.phylosophyit.homefinance.dao.model.TransactionType;
import ru.phylosophyit.homefinance.dao.HomeFinanceException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class TransactionRepository implements Repository<TransactionModel> {
    private final ConnectionSupplier connectionSupplier;

    public TransactionRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public TransactionModel findByID(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionmodel_tbl WHERE id = " + "'" + id + "'");
            return getTransactionModel(preparedStatement, false);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID TransactionRepository", e);
        }
    }

    public TransactionModel findByID(long id, boolean rec) {
        try (Connection connection = connectionSupplier.connection()) {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionmodel_tbl WHERE id = " + "'" + id + "'");
            return getTransactionModel(preparedStatement, rec);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID TransactionRepository", e);
        }
    }

    private TransactionModel getTransactionModel(PreparedStatement preparedStatement, boolean rec) throws SQLException {
        TransactionModel transactionModel = new TransactionModel();
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        transactionModel.setId(resultSet.getLong(1));
        transactionModel.setName(resultSet.getString(2));
        transactionModel.setAmount(resultSet.getBigDecimal(3));
        transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
        transactionModel.setTransactionType(new TransactionTypeRepository(connectionSupplier).findByID(resultSet.getLong(5)));
        transactionModel.setAccountUsersModel(new AccountUsersRepository(connectionSupplier).findByID(resultSet.getLong(6)));
        if (!rec) {
            transactionModel.setCategoryModels(getCategoryModels(transactionModel.getId()));
        } else {
            transactionModel.setCategoryModels(null);
        }
        preparedStatement.close();
        return transactionModel;
    }

    private Set<CategoryModel> getCategoryModels(long transactionId) {
        Connection connection = connectionSupplier.connection();
        CategoryRepository categoryRepository = new CategoryRepository(connectionSupplier);
        Set<CategoryModel> categoryModels = new HashSet<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement
                ("SELECT * FROM transaction_category_tbl WHERE transaction_id = ? ")) {
            preparedStatement.setLong(1, transactionId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CategoryModel categoryModel = categoryRepository.findByID(resultSet.getLong(2), true);
                categoryModels.add(categoryModel);
            }
            return categoryModels.isEmpty() ? null : categoryModels;
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID CategoryRepository", e);
        }
    }

    @Override
    public TransactionModel findByName(String name) {
        try (Connection connection = connectionSupplier.connection()) {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionmodel_tbl WHERE name = ? ");
            preparedStatement.setString(1, name);
            return getTransactionModel(preparedStatement, false);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByName TransactionRepository", e);
        }
    }

    @Override
    public Collection<TransactionModel> findAll() {
        Connection connection = connectionSupplier.connection();
        try (PreparedStatement preparedStatement = connection.prepareStatement
                ("SELECT * FROM transactionmodel_tbl")) {
            ArrayList<TransactionModel> transactionModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                TransactionModel transactionModel = new TransactionModel();
                transactionModel.setId(resultSet.getLong(1));
                transactionModel.setName(resultSet.getString(2));
                transactionModel.setAmount(resultSet.getBigDecimal(3));
                transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
                long transactionTypeId = resultSet.getLong(5);
                long accountUsersId = resultSet.getLong(6);
                TransactionType transactionType = new TransactionTypeRepository(connectionSupplier).findByID(transactionTypeId);
                AccountUsersModel accountUsersModel = new AccountUsersRepository(connectionSupplier).findByID(accountUsersId);
                transactionModel.setTransactionType(transactionType);
                transactionModel.setAccountUsersModel(accountUsersModel);
                transactionModel.setCategoryModels(getCategoryModels(transactionModel.getId()));
                transactionModels.add(transactionModel);
            }
            return transactionModels;
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll TransactionRepository", e);
        }
    }

    @Override
    public void remove(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("DELETE FROM transaction_category_tbl WHERE transaction_id = " + id);
                 PreparedStatement preparedStatement2 = connection.prepareStatement
                         ("DELETE FROM transactionmodel_tbl WHERE id = " + id)) {
                preparedStatement1.executeUpdate();
                preparedStatement2.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove TransactionRepository", e);
        }
    }

    @Override
    public TransactionModel save(TransactionModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transactionmodel_tbl (name, amount, profitorloss, type_id, account_id) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setBigDecimal(2, model.getAmount());
                preparedStatement.setBoolean(3, model.getProfitOrLoss());
                preparedStatement.setLong(4, model.getTransactionType().getId());
                preparedStatement.setLong(5, model.getAccountUsersModel().getId());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                PreparedStatement preparedStatement1 = connection.prepareStatement
                        ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?)");
                Set<CategoryModel> categoryModels = model.getCategoryModels();
                if (categoryModels != null) {
                    for (CategoryModel categoryModel : categoryModels) {
                        preparedStatement1.setLong(1, model.getId());
                        preparedStatement1.setLong(2, categoryModel.getId());
                        preparedStatement1.executeUpdate();
                    }
                }
                connection.commit();
                preparedStatement1.close();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save TransactionRepository", e);
        }
    }

    @Override
    public void update(TransactionModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transactionmodel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", amount = " + "'" + model.getAmount() + "'" +
                            ", profitorloss = " + "'" + model.getProfitOrLoss() + "'" +
                            ", type_id = " + "'" + model.getTransactionType().getId() + "'" +
                            ", account_id = " + "'" + model.getAccountUsersModel().getId() + "'" +
                            "WHERE ID = " + model.getId());
                 PreparedStatement preparedStatement1 = connection.prepareStatement
                         ("DELETE FROM transaction_category_tbl WHERE transaction_id = " + model.getId())
                 ) {
                preparedStatement.executeUpdate();
                Set<CategoryModel> categoryModels = model.getCategoryModels();
                preparedStatement1.executeUpdate();
                if (categoryModels != null) {
                    for (CategoryModel categoryModel : categoryModels) {
                        PreparedStatement preparedStatement2 = connection.prepareStatement
                                ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?)");
                        preparedStatement2.setLong(1,model.getId());
                        preparedStatement2.setLong(2,categoryModel.getId());
                        preparedStatement2.executeUpdate();
                    }
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in update TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in update TransactionRepository", e);
        }
    }
}

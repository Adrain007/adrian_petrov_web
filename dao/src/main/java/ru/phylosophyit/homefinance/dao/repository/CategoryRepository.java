package ru.phylosophyit.homefinance.dao.repository;

import ru.phylosophyit.homefinance.dao.HomeFinanceException;
import ru.phylosophyit.homefinance.dao.model.CategoryModel;
import ru.phylosophyit.homefinance.dao.model.ParentCategoryModel;
import ru.phylosophyit.homefinance.dao.model.TransactionModel;

import java.sql.*;
import java.util.*;

public class CategoryRepository implements Repository<CategoryModel> {

    private final ConnectionSupplier connectionSupplier;

    public CategoryRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public CategoryModel findByID(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categorymodel_tbl WHERE id = ? ");
            preparedStatement.setLong(1, id);
            return getCategoryModel(preparedStatement, false);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID CategoryRepository", e);
        }
    }

    public CategoryModel findByID(long id, boolean rec) {
        try (Connection connection = connectionSupplier.connection()) {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categorymodel_tbl WHERE id = ? ");
            preparedStatement.setLong(1, id);
            return getCategoryModel(preparedStatement, rec);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID CategoryRepository", e);
        }
    }

    private CategoryModel getCategoryModel(PreparedStatement preparedStatement, boolean rec) throws SQLException {
        CategoryModel categoryModel = new CategoryModel();
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        categoryModel.setId(resultSet.getLong(1));
        categoryModel.setNameCategory(resultSet.getString(2));
        long currencyId = resultSet.getLong(3);
        ParentCategoryModel parentCategoryModel = new ParentCategoryRepository(connectionSupplier).findByID(currencyId);
        categoryModel.setParentCategoryModel(parentCategoryModel);
        if (!rec) {
            categoryModel.setTransactionModels(getTransactionModels(categoryModel.getId()));
        } else {
            categoryModel.setTransactionModels(null);
        }
        preparedStatement.close();
        return categoryModel;
    }

    private Set<TransactionModel> getTransactionModels(long categoryId) {
        TransactionRepository transactionRepository = new TransactionRepository(connectionSupplier);
        Set<TransactionModel> transactionModels = new HashSet<>();
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM transaction_category_tbl WHERE category_id = ? ")) {
            preparedStatement.setLong(1, categoryId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                TransactionModel transactionModel = transactionRepository.findByID(resultSet.getLong(1), true);
                transactionModels.add(transactionModel);
            }
            return transactionModels.isEmpty() ? null : transactionModels;
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID CategoryRepository", e);
        }
    }

    @Override
    public CategoryModel findByName(String name) {

        try (Connection connection = connectionSupplier.connection()) {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categorymodel_tbl WHERE name = ? ");
            preparedStatement.setString(1, name);
            return getCategoryModel(preparedStatement, false);
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByName CategoryRepository", e);
        }
    }

    @Override
    public Collection<CategoryModel> findAll() {
        try (Connection connection = connectionSupplier.connection();
             PreparedStatement preparedStatement = connection.prepareStatement
                     ("SELECT * FROM categorymodel_tbl")) {
            ArrayList<CategoryModel> categoryModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CategoryModel categoryModel = new CategoryModel();
                categoryModel.setId(resultSet.getLong(1));
                categoryModel.setNameCategory(resultSet.getString(2));
                long currencyId = resultSet.getLong(3);
                ParentCategoryModel parentCategoryModel = new ParentCategoryRepository(connectionSupplier).findByID(currencyId);
                categoryModel.setParentCategoryModel(parentCategoryModel);
                categoryModel.setTransactionModels(getTransactionModels(categoryModel.getId()));
                categoryModels.add(categoryModel);
            }
            return categoryModels;
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll CategoryRepository", e);
        }
    }

    @Override
    public void remove(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_category_tbl WHERE category_id = " + id);
                 PreparedStatement preparedStatement1 = connection.prepareStatement
                         ("DELETE FROM categorymodel_tbl WHERE id = " + id)) {
                preparedStatement.executeUpdate();
                preparedStatement1.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove CategoryRepository", e);
        }
    }

    @Override
    public CategoryModel save(CategoryModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO categorymodel_tbl (name, parent_id) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getNameCategory());
                preparedStatement.setLong(2, model.getParentCategoryModel().getId());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                Set<TransactionModel> transactionModels = model.getTransactionModels();
                if (transactionModels != null) {
                    for (TransactionModel transactionModel : transactionModels) {
                        PreparedStatement preparedStatement1 = connection.prepareStatement
                                ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?)");
                        preparedStatement1.setLong(1, transactionModel.getId());
                        preparedStatement1.setLong(2, model.getId());
                        preparedStatement1.executeUpdate();
                    }
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save CategoryRepository", e);
        }
    }

    @Override
    public void update(CategoryModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE categorymodel_tbl SET name = " + "'" + model.getNameCategory() + "'" +
                            ", parent_id = " + "'" + model.getParentCategoryModel().getId() + "'" +
                            "WHERE ID = " + model.getId())) {
                new ParentCategoryRepository(connectionSupplier).update(model.getParentCategoryModel());
                preparedStatement.executeUpdate();
                Set<TransactionModel> transactionModels = model.getTransactionModels();
                PreparedStatement preparedStatement1 = connection.prepareStatement
                        ("DELETE FROM transaction_category_tbl WHERE category_id = " + model.getId());
                preparedStatement1.executeUpdate();
                if (transactionModels != null) {
                    for (TransactionModel transactionModel : transactionModels) {
                        preparedStatement1 = connection.prepareStatement
                                ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?)");
                        preparedStatement1.setLong(1, transactionModel.getId());
                        preparedStatement1.setLong(2, model.getId());
                        preparedStatement1.executeUpdate();
                    }
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in update CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in update CategoryRepository", e);
        }
    }
}

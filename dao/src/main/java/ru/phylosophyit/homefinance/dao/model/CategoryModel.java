package ru.phylosophyit.homefinance.dao.model;


import java.util.Objects;
import java.util.Set;

public class CategoryModel {
    private long id;
    private String nameCategory;
    private ParentCategoryModel parentCategoryModel;
    private Set<TransactionModel> transactionModels;   //ManyToMany

    public CategoryModel() {
    }

    public CategoryModel(String nameCategory, ParentCategoryModel parentCategoryModel) {
        this.nameCategory = nameCategory;
        this.parentCategoryModel = parentCategoryModel;
    }

    public CategoryModel(String nameCategory, ParentCategoryModel parentCategoryModel, Set<TransactionModel> transactionModels) {
        this.nameCategory = nameCategory;
        this.parentCategoryModel = parentCategoryModel;
        this.transactionModels = transactionModels;
    }

    public CategoryModel(long id, String nameCategory, ParentCategoryModel parentCategoryModel, Set<TransactionModel> transactionModels) {
        this.id = id;
        this.nameCategory = nameCategory;
        this.parentCategoryModel = parentCategoryModel;
        this.transactionModels = transactionModels;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public ParentCategoryModel getParentCategoryModel() {
        return parentCategoryModel;
    }

    public void setParentCategoryModel(ParentCategoryModel parentCategoryModel) {
        this.parentCategoryModel = parentCategoryModel;
    }

    public Set<TransactionModel> getTransactionModels() {
        return transactionModels;
    }

    public void setTransactionModels(Set<TransactionModel> transactionModels) {
        this.transactionModels = transactionModels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryModel that = (CategoryModel) o;
        return id == that.id &&
                Objects.equals(parentCategoryModel, that.parentCategoryModel) &&
                Objects.equals(nameCategory, that.nameCategory) &&
                Objects.equals(transactionModels, that.transactionModels);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameCategory, parentCategoryModel, transactionModels);
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id=" + id +
                ", nameCategory='" + nameCategory + '\'' +
                ", parentCategoryModel=" + parentCategoryModel +
                ", transactionModels=" + transactionModels +
                '}';
    }
}
